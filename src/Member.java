import java.util.ArrayList;


public class Member {
	
	private String name;
	private ArrayList<Publication> record = new ArrayList<Publication>();
	private static int lastMemberID = 0;
	private int memberID;
	
	public Member(String name) {
		this.memberID = ++lastMemberID;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addRecord(Publication pub) {
		this.record.add(pub);
	}
	
	public int getMemberID() {
		return memberID;
	}

	public ArrayList<Publication> getRecord() {
		return record;
	}

}
