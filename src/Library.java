import java.util.ArrayList;


public class Library {
	
	private ArrayList<Publication> books;
	private ArrayList<Member> members;
	
	public Library() {
		books = new ArrayList<Publication>();
		members = new ArrayList<Member>();
	}
	
	public void borrowBook(Member member, String title) {
		for (Publication pub:books) {
			if (pub.getTitle().equals(title) && pub.getStatus() != false) {
				members.add(member);
				member.addRecord(pub);
				pub.addRecord(member.getName());
			}
		}
	}
	
	public void returnBook(Member member, String title) {
		for (Publication pub:member.getRecord()) {
			if (pub.getTitle().equals(title)) {
				member.getRecord().remove(pub);
			}
		}
	}
	
	public void addBook(Publication pub) {
		books.add(pub);
	}
}
