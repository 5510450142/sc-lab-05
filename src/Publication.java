import java.util.ArrayList;


public class Publication {
	
	private String title;
	private String author;
	private String category;
	private String publisher;
	private Boolean status;
	private ArrayList<String> record = new ArrayList<String>();
	
	public Publication(String title, String author, Boolean status) {
		this.title = title;
		this.author = author;
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public String getCategory() {
		return category;
	}
	
	public ArrayList<String> getRecord() {
		return record;
	}
	
	public String getPublisher() {
		return publisher;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	public void addRecord(String name) {
		this.record.add(name);
	}
}
